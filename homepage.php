<?php
/*
Template Name: Página de inicio
*/
    get_template_part('includes/header'); 
    bk_main_before();
?>
<div class="owl-carousel owl-theme">
    <div class="item">
        <img src="<?php bloginfo('template_directory'); ?>/assets/img/bg1.jpg" alt="Posada Casalta" class="w-100">
    </div>
    <div class="item">
        <img src="<?php bloginfo('template_directory'); ?>/assets/img/bg2.jpg" alt="Posada Casalta" class="w-100">
    </div>
    <div class="item">
        <img src="<?php bloginfo('template_directory'); ?>/assets/img/bg3.jpg" alt="Posada Casalta" class="w-100">
    </div>
</div>
<?php get_template_part('loops/page-content'); ?>
    <section class="container">
        <div class="row">
            <div class="col-12">
                <h2>La Posada</h2>
                <p>Consta de una casa colonial de tres habitaciones matrimoniales con baño propio, calentador, cocina, estacionamiento, habitación múltiple, mini shop y todas las comodidades necesarias para el turista.</p>
            </div>
        </div>
    </section>
    <section class="container">
        <div class="row">
        <div class="col-12">
                <h2>El Clima</h2>
                <p>Por estar entre montañas que constituyen la cordillera Andina, la temperatura oscila entre 10 grados centígrados para el mes de Diciembre y 22 grados centígrados para el resto del año, en la actualidad es muy variable, pero siempre fresco</p>
            </div>
        </div>
    </section>
    <section class="container">
        <div class="row">
            <div class="col-12">
                <h2>Hidrografia</h2>
                <p>Hidrografia. El Valle del Rio Tocuyo, principal Rio que se ubica a unos 25 Km de la población de Guarico, por lo tanto allí se encuentran las quebradas las limas y quebrada Burón las cuales son vertientes al Rio Tocuyo, además otros acuíferos que llegan al Rio Tocuyo.</p>
            </div>
        </div>
    </section>
    <section class="container">
        <div class="row">
            <div class="col-12"></div>
        </div>
    </section>

<?php 
    bk_main_after();
    get_template_part('includes/footer'); 
?>
